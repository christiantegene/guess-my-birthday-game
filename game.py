from random import randint
#prompt user for their name
name = input("Hi! What is your name? ")

#randomly generate a number for a month of the year
rand_num_str = randint(1, 12)
rand_month = int(rand_num_str)

rand_num_str = randint(1924, 2004)
rand_year = int(rand_num_str)

for i in range(1,6):
    if i == 1:
        print("Hi ", name, "!")

    print("Guess", i , ": Were you born in ", int(randint(1,12)), "/", int(randint(1924,2004)), "?")

    answer = input("yes or no? ")

    if answer == "yes":
        print("I knew it!")
        break
    elif i == 5:
        print("I have other things to do. Good bye.")
    else:
        print("Drat! Lemme try again!")
